import { Component, OnInit, Input } from '@angular/core';
import { GithubPullRequest } from '../githubexplorer-service/githubPullRequest';
import { GithubexplorerService } from '../githubexplorer-service/githubexplorer.service';

@Component({
  selector: 'app-pull-request-view',
  templateUrl: './pull-request-view.component.html',
  styleUrls: ['./pull-request-view.component.css']
})
export class PullRequestViewComponent implements OnInit {
  pullRequests: GithubPullRequest[];

  constructor(private githubexplorerService: GithubexplorerService) { }

  ngOnInit() {
  }

  @Input()
  set username(username: string) {
    if (username === null) {
      return;
    }
    this.githubexplorerService.getPullRequests(username).subscribe(pullRequests => this.pullRequests = pullRequests);
  }
}
