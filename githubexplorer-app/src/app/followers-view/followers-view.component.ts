import { Component, OnInit, Input } from '@angular/core';
import { GithubUser } from '../githubexplorer-service/githubUser';
import { GithubexplorerService } from '../githubexplorer-service/githubexplorer.service';

@Component({
  selector: 'app-followers-view',
  templateUrl: './followers-view.component.html',
  styleUrls: ['./followers-view.component.css']
})
export class FollowersViewComponent implements OnInit {
  followers: GithubUser[];

  constructor(private githubexplorerService: GithubexplorerService) { }

  ngOnInit() {
  }

  @Input()
  set username(username: string) {
    if (username === null) {
      return;
    }
    this.githubexplorerService.getFollowers(username).subscribe(followers => this.followers = followers);
  }
}
