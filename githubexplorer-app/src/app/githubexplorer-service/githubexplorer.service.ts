import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError, map, tap } from 'rxjs/operators';
import { GithubUser } from './githubUser';
import { GithubPullRequest } from './githubPullRequest';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class GithubexplorerService {
  private githubexplorerUrl = 'http://localhost:5000/Search';

  constructor(private http: HttpClient) { }

  getUsers(username: string): Observable<GithubUser[]> {
    return this.http.get<GithubUser[]>(`${this.githubexplorerUrl}/Users?username=${username}`);
  }

  getFollowers(login: string): Observable<GithubUser[]> {
    return this.http.get<GithubUser[]>(`${this.githubexplorerUrl}/Followers?login=${login}`);
  }

  getPullRequests(login: string): Observable<GithubPullRequest[]> {
    return this.http.get<GithubPullRequest[]>(`${this.githubexplorerUrl}/PullRequests?login=${login}`);
  }
}
