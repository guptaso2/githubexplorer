import { TestBed, inject } from '@angular/core/testing';
import { GithubexplorerService } from './githubexplorer.service';


describe('GithubexplorerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GithubexplorerService]
    });
  });

  it('should be created', inject([GithubexplorerService], (service: GithubexplorerService) => {
    expect(service).toBeTruthy();
  }));
});
