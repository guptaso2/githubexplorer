export interface GithubUser {
    id: number;
    login: string;
    avatarUrl: string;
}
