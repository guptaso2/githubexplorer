export interface GithubPullRequest {
    title: string;
    url: string;
}
