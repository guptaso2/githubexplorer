import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'


import { AppComponent } from './app.component';
import { UserDropdownComponent } from './user-dropdown/user-dropdown.component';
import { GithubexplorerService } from './githubexplorer-service/githubexplorer.service';
import { FollowersViewComponent } from './followers-view/followers-view.component';
import { PullRequestViewComponent } from './pull-request-view/pull-request-view.component';

@NgModule({
  declarations: [
    AppComponent,
    UserDropdownComponent,
    FollowersViewComponent,
    PullRequestViewComponent
],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    GithubexplorerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
