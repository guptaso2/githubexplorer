import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GithubUser } from '../githubexplorer-service/githubUser';
import { GithubexplorerService } from '../githubexplorer-service/githubexplorer.service';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-user-dropdown',
  templateUrl: './user-dropdown.component.html',
  styleUrls: ['./user-dropdown.component.css']
})
export class UserDropdownComponent implements OnInit {
  @Output() emitUsername = new EventEmitter<string>();
  username = '';
  users$: Observable<GithubUser[]>;
  focus = false;
  private searchTerms = new Subject<string>();

  constructor(private githubexplorerService: GithubexplorerService) { }

  ngOnInit() {
    this.users$ = this.searchTerms.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((term: string) => this.githubexplorerService.getUsers(term))
    );
  }

  setUsername(username: string) {
    this.username = username;
    this.searchTerms.next(username);
  }

  clickUsername(username: string) {
    this.focus = false;
    this.emitUsername.emit(username);
    this.setUsername(username);
  }

  setQuery(item) { }
}
