namespace githubexplorer_server.Models
{
    public class GithubPullRequest
    {
        public string Title { get; set; }
        public string Url { get; set; }
    }
}