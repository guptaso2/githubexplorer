namespace githubexplorer_server.Models
{
    public class GithubUser
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string AvatarUrl { get; set; }
    }
}