using Microsoft.AspNetCore.Mvc;
using System.Text.Encodings.Web;
using Octokit;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;

namespace githubexplorer_server.Controllers
{
    public class SearchController : Controller
    {
        private readonly GitHubClient _githubClient;

        public SearchController(GitHubClient githubClient)
        {
            _githubClient = githubClient;
        }

        public string Index()
        {
            return "Nothing to see here.";
        }

        public async Task<List<Models.GithubUser>> Users(string username)
        {
            if (username == null) {
                return new List<Models.GithubUser>();
            }

            var searchRequest = new SearchUsersRequest(username)
            {
                AccountType = AccountSearchType.User,
                In = new[] { UserInQualifier.Username }
            };
            searchRequest.PerPage = 10;

            var userResults = await _githubClient.Search.SearchUsers(searchRequest);
            var usernameResults = new List<Models.GithubUser>();
            var items = userResults.Items;
            foreach (var item in items)
            {
                var githubUser = new Models.GithubUser(){ 
                    Id = item.Id, 
                    Login = item.Login,
                    AvatarUrl = item.AvatarUrl
                };
                usernameResults.Add(githubUser);
            }

            return usernameResults;
        }

        public async Task<List<Models.GithubUser>> Followers(string login)
        {
            if (login == null) {
                return new List<Models.GithubUser>();
            }

            var followersResult = await _githubClient.User.Followers.GetAllFollowing(login);

            var followers = new List<Models.GithubUser>();
            foreach (var user in followersResult)
            {
                followers.Add(item: new Models.GithubUser(){
                    Id = user.Id,
                    Login = user.Login, 
                    AvatarUrl = user.AvatarUrl 
                });
            }

            return followers;
        }

        public async Task<List<Models.GithubPullRequest>> PullRequests(string login)
        {
            if (login == null) {
                return new List<Models.GithubPullRequest>();
            }

            var issuesRequest = new SearchIssuesRequest()
            {
                Type = IssueTypeQualifier.PullRequest,
                State = ItemState.Open,
                Author = login
            };
            var issuesResult = await _githubClient.Search.SearchIssues(issuesRequest);
            
            var prList = new List<Models.GithubPullRequest>();
            foreach (var issue in issuesResult.Items)
            {
                var pr = new Models.GithubPullRequest(){
                    Title = issue.Title,
                    Url = issue.HtmlUrl
                };
                prList.Add(pr);
            }

            return prList;
        }

    }
}