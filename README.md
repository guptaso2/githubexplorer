# GithubExplorer

Run the `githubexplorer-server` project in Debug mode from Visual Studio.

Naviate to the `githubexplorer-app` folder and run the command `ng serve --open`

The browser should automatically navigate to http://localhost:4200